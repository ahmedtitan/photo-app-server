FROM alpine:latest
RUN apk add --no-cache nodejs npm

WORKDIR /photo-app-server

COPY . /photo-app-server

RUN npm install --silent

EXPOSE 4000

ENTRYPOINT ["npm"]

CMD ["start"]
