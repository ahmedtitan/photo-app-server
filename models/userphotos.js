'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userPhotos extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  userPhotos.init(
    {
      userPhotoID: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      authorID: DataTypes.STRING,
      imageUrl: DataTypes.STRING,
      photoSequence: DataTypes.NUMERIC,
    },
    {
      sequelize,
      modelName: 'userPhotos',
    },
  );
  return userPhotos;
};
