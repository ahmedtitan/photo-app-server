import '@babel/register';
import '@babel/polyfill';
import app from './app';

app.listen(process.env.PORT, () =>
  console.log(`Server listening on port ${process.env.PORT}! Environment : ${process.env.NODE_ENV}`.cyan.bold),
);
