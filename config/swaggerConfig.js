//https://swagger.io/specification/#infoObject

const options = {
  basePath: '/',
  swaggerDefinition: {
    openapi: '3.0.0',
    info: {
      description: 'Auto generated API documentation',
      swagger: '2.0',
      title: 'Photo App API Documentation',
      version: '1.0.0',
    },
  },
  // List of files to be processes. You can also set globs './routes/*.js'
  apis: ['./src/routes/*.js'],
};

export default options;
