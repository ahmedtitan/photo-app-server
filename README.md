# prerequisite

1. Nodejs
2. postgres db.

   If you want to run this with mysql
   set "dialect": "mysql" in config.json inside config folder.
   and install mysql2 npm package

   for More info: https://sequelize.org/master/manual/getting-started.html

# Before run the project

3. create a db with the name of photo_db_dev.
4. instll dependencies
   > npm install
5. set NODE_env=development:

   linux:

   > export NODE_ENV=development

   windows:

   > SET NODE_ENV=development

6. run db migration command
   > npx sequelize-cli db:migrate

# How to run the project

1. To run the project
   > npm start
2. To run the test
   > npm run test

### code explaination and used packages

- This is an express js project
- Routes, controllers and services are added.
- Joi validation is used as middleware to validate API request bodies.
- All DB queries are haandled through sequelize
- VS code configuration, eslint and prettier configuration are added.
- jest and supertest installed to run the test cases.
- All the test cases are in aapp.test.js file

### Docker run

Docker file added.
Docker container port: 4000

Create docker container

> docker build -t photo-app-server:latest .

Run docker container

> docker run -it -d -p 4000:4000 photo-app-server:latest
