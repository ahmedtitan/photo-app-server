import axios from 'axios';
import sequelize from 'sequelize';
import models from '../../models';

export const fetchALLPhotosService = async () => {
  try {
    const photos = await axios.get(
      'https://dev-pb-apps.s3-eu-west-1.amazonaws.com/collection/CHhASmTpKjaHyAsSaauThRqMMjWanYkQ.json',
    );
    return Promise.resolve(photos.data);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const savePhotosService = async (authorID, photos) => {
  const transaction = await models.sequelize.transaction();
  try {
    await models.userPhotos.destroy({ where: { authorID } });
    await models.userPhotos.bulkCreate(photos.map((photo) => ({ ...photo, authorID })));

    // commit the db changes
    await transaction.commit();
  } catch (error) {
    // Roll back all the db changes
    await transaction.rollback();
    return Promise.reject(error);
  }
};

export const fetchPhotosBuAuthorIdService = async (authorID) =>
  models.userPhotos.findAll({
    where: {
      authorID,
    },
  });
