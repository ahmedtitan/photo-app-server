import { RES_CODE } from '../common/constants';

const { errorCode, successCode } = RES_CODE;

const error = ({ res, message, status, error }) => {
  if (error?.response) {
    //axios response
    res
      .status(error.response.status || errorCode.internalServerError)
      .send({ success: false, ...error.response?.data });
  } else {
    //unknown response
    res.status(status || errorCode.internalServerError).send({
      success: false,
      message: message || error?.message || 'Something went wrong',
      error: error || null,
    });
  }
};

const success = ({ res, message, data, status }) => {
  if (data) {
    //Success with data
    return res.status(status || successCode.ok).send({
      success: true,
      message,
      data,
    });
  }
  //Success without data
  res.status(status || successCode.ok).send({
    success: true,
    message,
  });
};

export default {
  error,
  success,
};
