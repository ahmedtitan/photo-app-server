export const RES_CODE = {
  errorCode: {
    badRequest: 400,
    notVerified: 403,
    validationError: 422,
    unauthorized: 401,
    notFound: 404,
    conflict: 409,
    internalServerError: 500,
  },
  successCode: {
    ok: 200,
    nonAuthoritativeInformation: 203,
    created: 201,
    noContent: 204,
  },
};
