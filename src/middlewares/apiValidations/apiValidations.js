import response from '../../lib/response';
import errorCodes from '../../lib/responseCodes';
import Joi from 'joi';

const { errorCode } = errorCodes;

//------------------------------------ Validate save photo route route ------------------------------------

const validatePutPhotochema = Joi.object({
  authorID: Joi.string().required(),
  photos: Joi.array()
    .items(
      Joi.object({
        imageUrl: Joi.string().required(),
        photoSequence: Joi.number().required(),
      }),
    )
    .required()
    .length(9),
});

const validategetPutPhoto = async (req, res, next) => {
  try {
    await validatePutPhotochema.validateAsync(req.body);
    next();
  } catch (err) {
    response.error({
      res,
      message: err?.details ? err.details[0].message : 'Invalid input data',
      status: errorCode.validationError,
      err,
    });
  }
};

export default { validategetPutPhoto };
