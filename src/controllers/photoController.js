import response from '../lib/response';
import { RES_CODE } from '../common/constants';
import { fetchALLPhotosService, fetchPhotosBuAuthorIdService, savePhotosService } from '../services/photo.service';

const { successCode, errorCode } = RES_CODE;

export const getAllUserPhotos = async (req, res) => {
  try {
    return response.success({
      res,
      message: 'Photos fetched',
      data: await fetchALLPhotosService(),
      status: successCode.ok,
    });
  } catch (error) {
    // Response for unknown errors
    response.error({ res, error });
  }
};

export const savePhotosByauthorID = async (req, res) => {
  const { authorID, photos } = req.body;

  try {
    await savePhotosService(authorID, photos);

    return response.success({
      res,
      message: 'Photo sequence saved',
      status: successCode.created,
    });
  } catch (error) {
    // Response for unknown errors
    response.error({ res, error });
  }
};
export const getPhotosByauthorID = async (req, res) => {
  const { authorID } = req.params;
  const photos = (await fetchPhotosBuAuthorIdService(authorID)) || [];

  try {
    return response.success({
      res,
      message: 'Photos fetched',
      data: photos.sort((a, b) => a.photoSequence - b.photoSequence),
      status: successCode.ok,
    });
  } catch (error) {
    // Response for unknown errors
    response.error({ res, error });
  }
};

export default {
  getAllUserPhotos,
  savePhotosByauthorID,
  getPhotosByauthorID,
};
