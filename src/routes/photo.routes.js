import express from 'express';

// Validation middleware
import { apiValidations } from '../middlewares';

// Controller import
import { photoController } from '../controllers';

const router = express.Router(); // Express Router instance

// Route implementations

/**
 * @swagger
 * /api/v1/pa/photo:
 *  get:
 *      tags:
 *          - Photo Routes
 *      name: Photo
 *      produces:
 *          - application/json
 *      consumes:
 *          - application/json
 *      summary: Fetch all photos
 *      responses:
 *          '200':
 *              description: Fetched photos successfully.
 *              application/json:
 */
router.get('/', photoController.getAllUserPhotos);

/**
 * @swagger
 * /api/v1/pa/photo/{authorID}:
 *  get:
 *      tags:
 *          - Photo Routes
 *      name: Photo
 *      produces:
 *          - application/json
 *      consumes:
 *          - application/json
 *      summary: Fetch All photos selected by the user
 *      parameters:
 *          - name: authorID
 *            in: path
 *            type: string
 *            format: string
 *            require: true
 *            example:
 *              101
 *      responses:
 *          '200':
 *              description: Successfully Fetched photos selected by the user.
 *              application/json:
 */
router.get('/:authorID', photoController.getPhotosByauthorID);

/**
 * @swagger
 * /api/v1/pa/photo:
 *  put:
 *      tags:
 *          - Photo Routes
 *      name: Photo
 *      produces:
 *          - application/json
 *      consumes:
 *          - application/json
 *      summary: Update users grid.
 *      requestBody:
 *         content:
 *            application/json:
 *               schema:
 *                  type: object
 *                  properties:
 *                     authorID:
 *                        type: string
 *                        example:
 *                           101
 *                     photos:
 *                        type: array
 *                        items:
 *                           type: object
 *                           properties:
 *                              imageUrl:
 *                                  type: string
 *                              photoSequence:
 *                                  type: number
 *      responses:
 *          '200':
 *              description: Password reset success
 *              application/json:
 */

router.put('/', apiValidations.validategetPutPhoto, photoController.savePhotosByauthorID);

export default router;
