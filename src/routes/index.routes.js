import express from 'express';

//Route file imports
import photoRouter from './photo.routes';

// Express Router instance
const router = express.Router();

// sample url: http://localhost:4000/api/v1/pa
const baseUrlV1 = process.env.BASE_URL_V1;

// Test route to check server status
router.get(`/`, (req, res) => {
  res.send({ server: 'App online' });
});

// photo routes: http://localhost:4000/api/v1/pa/photo/*
router.use(`${baseUrlV1}/photo`, photoRouter);

module.exports = router;
