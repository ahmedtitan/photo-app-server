import 'babel-polyfill';
import request from 'supertest';
import app from './app';

beforeAll((done) => {
  done();
});
afterAll((done) => {
  done();
});

describe('GET /photo', () => {
  describe('Fetch all photos from placeimg.com', () => {
    test('should fetch all images', async () => {
      const response = await request(app).get('/api/v1/pa/photo');
      expect(response.statusCode).toBe(200);
    });
  });
});

describe('PUT /photo', () => {
  test('should Save photos with valid data', async () => {
    const response = await request(app)
      .put('/api/v1/pa/photo')
      .send({
        authorID: '101',
        photos: [
          { imageUrl: 'https://picsum.photos/id/20', photoSequence: 0 },
          { imageUrl: 'https://picsum.photos/id/16', photoSequence: 1 },
          { imageUrl: 'https://picsum.photos/id/40', photoSequence: 2 },
          { imageUrl: 'https://picsum.photos/id/32', photoSequence: 3 },
          { imageUrl: 'https://picsum.photos/id/52', photoSequence: 4 },
          { imageUrl: 'https://picsum.photos/id/8', photoSequence: 5 },
          { imageUrl: 'https://picsum.photos/id/4', photoSequence: 6 },
          { imageUrl: 'https://picsum.photos/id/132', photoSequence: 7 },
          { imageUrl: 'https://picsum.photos/id/0', photoSequence: 8 },
        ],
      });
    expect(response.statusCode).toBe(201);
  });

  test('should return 422 if authorID is not supplied', async () => {
    const response = await request(app)
      .put('/api/v1/pa/photo')
      .send({
        photos: [{ imageUrl: 'https://picsum.photos/id/20', photoSequence: 0 }],
      });
    expect(response.statusCode).toBe(422);
  });
});

describe('GET /photo/:authorID', () => {
  it('should fetch all images by user id', async () => {
    const response = await request(app).get('/api/v1/pa/photo/101');
    expect(response.statusCode).toBe(200);
  });
});
