import 'dotenv/config';
import 'colors';
import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import swaggerJsDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

import indexRouter from './src/routes/index.routes';
import swaggerConfig from './config/swaggerConfig';

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(morgan('dev'));

// All available routes
app.use('/', indexRouter);

//Auto generated swagger endppoint
app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerJsDoc(swaggerConfig)));

// catch 404 and forward to error handler
app.use((req, res, next) => {
  res.status(404).send('Not found');
});

export default app;
